using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveLoadData : MonoBehaviour
{
    //public GameObject AnimWindow;
    //Animator animWindowMator;
    public Transform PlayerTransform;
    private float playerPosX = 0;
    private float playerPosY = 0;

    void Start()
    {
        //AnimWindow = GameObject.Find("AnimWindow");
       // animWindowMator = AnimWindow.GetComponent<Animator>();
        //AnimWindow.SetActive(false);
    }

    void Update()
    {
        playerPosX = PlayerTransform.position.x;
        playerPosY = PlayerTransform.position.y;
    }

    public void SavingData()
    {
        PlayerPrefs.SetFloat("posX", playerPosX);
        PlayerPrefs.SetFloat("posY", playerPosY);
        //StartCoroutine(waitingForAnimation("Save"));

        PlayerPrefs.Save();
    }
    public void LoadingData()
    {
        if(PlayerPrefs.HasKey("posX")|| PlayerPrefs.HasKey("posY"))
        {
            playerPosX = PlayerPrefs.GetFloat("posX");
            playerPosY = PlayerPrefs.GetFloat("posY");
            PlayerTransform.position = (new Vector2(playerPosX, playerPosY));
        }
        else
            NewGame();
    }

    public void NewGame()
    {
        PlayerTransform.position = (new Vector2(0,0));
    }

    /*public IEnumerator waitingForAnimation(string trigger)
    {
        AnimWindow.SetActive(true);
        animWindowMator.SetTrigger(trigger);
        yield return new WaitForSeconds(1f);
        AnimWindow.SetActive(false);
    }*/
}
