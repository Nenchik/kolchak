using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public Animator animator;
    public float speed;
    private Rigidbody2D rb;
    private Vector2 direction;
    public VectorValue pos;

    // Start is called before the first frame update
    void Start()
    {
        //transform.position = pos.initialValue;
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        direction.x = Input.GetAxisRaw("Horizontal");
        direction.y = Input.GetAxisRaw("Vertical");

        animator.SetFloat("Horizontal", direction.x);
        animator.SetFloat("Vertical", direction.y);
        animator.SetFloat("Speed", direction.sqrMagnitude);
    
    }

    void FixedUpdate()
    {
        
        rb.MovePosition(rb.position + direction * speed * Time.fixedDeltaTime);
    }
}
